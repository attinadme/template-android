Readme
------

This is the Attinad template project for [Android](https://en.wikipedia.org/wiki/Android_(operating_system)) apps. The goal of this project is to provide a starting point from which other apps can start their implementations. Some of the packages and base classes for the project have been pre-populated in the template. The project also includes the following 3<sup>rd</sup> party components:

- Retrofit 2.1
- Glide 3.6.1
- Disklrucache 2.0.2
- lz4 1.1.1
- okhttp 3.6.0
- gson 2.7

We recommend that you follow a feature/module based package system. Instead of having packages for activities, views, managers etc. which would cause components related to a single feature to be distributed all over the place, consider having packages for whole features, and all components related to the features can be placed inside that single package. This will greatly improve readability and maintainability as well as reduce developer effort. Towards this you can see two empty packages already in the project titled **feature1** and **feature2**

For an example of how to use the service classes, please refer to the servicesample package