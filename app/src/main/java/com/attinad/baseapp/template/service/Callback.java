package com.attinad.baseapp.template.service;

/**
 * The Callback interface is used throughout the Base application for services that require
 * asynchronous processing. It is recommended to utilize this callback interface as well in your
 * project, to maintain a common standard for handling asynchronous requests.
 *
 * @param <T> The expected response type
 *
 * @author SayoojO
 * @since BaseApp
 *
 * Copyright (c) 2017 Attinad Softwares Pvt Lmt. All rights reserved.
 */

public interface Callback<T> {

    /**
     * Execute the callback with response object.
     *
     * @param response Response object for the callback
     */
    void execute(final T response);

}
