package com.attinad.baseapp.template.cache;

import com.attinad.baseapp.template.util.*;

/**
 * An abstract cache key generator class for generating cache keys to be used in both the MemCache and FileCache.
 * The generateCacheKey method takes in the request endpoint, path and parameters to be sent and generates a Base64 string.
 *
 * @author SayoojO
 * @since BaseApp
 */
public final class CacheKeyGenerator {

    /**
     * Generates the cache key which is a Base64 String, from the specified parameters.
     *
     * @param endpoint The API endpoint
     * @param path The path to the API method called
     * @param parameters Request parameters
     * @return A base64 String
     */
    public static String generateCacheKey(final String endpoint, final String path, final String parameters) {
        final StringBuilder keyStr = new StringBuilder();
        keyStr.append(endpoint);
        keyStr.append(path);

        if (parameters != null) {
            keyStr.append('?');
            keyStr.append(parameters.toString());
        }
        Hash.md5Hash(keyStr.toString());
        return Hash.md5Hash(keyStr.toString());//Base64.encodeToString(keyStr.toString().getBytes(Charset.defaultCharset()), 0);
    }


    private CacheKeyGenerator() {
        // Not a proper class...
    }
}