package com.attinad.baseapp.template.service;

import android.content.Context;

import com.attinad.baseapp.template.servicesample.DataServiceApiInterface;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Rest client holder for holding different api clients
 *
 * @author SayoojO
 * @since BaseApp
 *
 * Copyright (c) 2017 Attinad Softwares Pvt Lmt. All rights reserved.
 */

public class RestClientHolder {

    private static volatile RestClientHolder instance;
    private final Context context;
    private Gson gson;

    // Service Clients
    private DataServiceApiInterface dataServiceClient;


    /**
     * Returns the singleton instance of {@link RestClientHolder}.
     *
     * @param context The application Context
     * @return The @link {@link RestClientHolder}} instance
     */
    public static RestClientHolder getInstance(final Context context) {
        RestClientHolder holder = instance;
        if (holder == null) {
            synchronized (ServiceHolder.class) {
                holder = instance;
                if (holder == null) {
                    holder = new RestClientHolder(context);
                    instance = holder;
                }
            }
        }
        return holder;
    }


    /**
     * Function to return the Dummy Data service API Client
     * @param baseUrl
     * @return DataServiceApiInterface
     */
    public DataServiceApiInterface getDataServiceClient(final String baseUrl){
        if (dataServiceClient == null) {
            dataServiceClient = getApiClient(baseUrl).create(DataServiceApiInterface.class);
        }
        return dataServiceClient;
    }







    /**
     * Method for initializing the retrofit api client
     * @param baseUrl
     * @return retrofit client instance
     */
    private Retrofit getApiClient(final String baseUrl){
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                //.client(getRequestInterceptor())
                .build();
    }


    private OkHttpClient getRequestInterceptor(){
        OkHttpClient client = new OkHttpClient();
        client.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Response response = chain.proceed(chain.request());

                // Do anything with response here

                return response;
            }
        });

        return client;
    }


    /**
     * Private Constructor for restricting unwanted instance creation.
     *
     * @param context
     */
    private RestClientHolder(Context context) {
        this.context = context;
        gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
    }

}
