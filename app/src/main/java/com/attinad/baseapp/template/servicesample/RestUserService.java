package com.attinad.baseapp.template.servicesample;

import android.content.Context;

import com.attinad.baseapp.template.service.RestClientHolder;
import com.attinad.baseapp.template.exception.RestError;
import com.attinad.baseapp.template.service.Callback;
import com.attinad.baseapp.template.service.UserService;

import retrofit2.Call;
import retrofit2.Response;

/**
 * An Sample implementation of {@link UserService} using REST Api
 *
 * @author SayoojO
 * @since BaseApp
 * <p>
 * Copyright (c) 2017 Attinad Softwares Pvt Lmt. All rights reserved.
 */

public class RestUserService implements UserService {

    private String baseUrl;

    public RestUserService(final String baseUrl){
        this.baseUrl = baseUrl;
    }

    @Override
    public void createUser(Context context, String userName, final Callback<DummyUser> onSuccess,
                           final Callback<RestError> onFailure) {

        DummyUser user = new DummyUser(userName);
        Call<DummyUser> call = RestClientHolder.getInstance(context).getDataServiceClient(baseUrl).createUser(user);

        call.enqueue(new retrofit2.Callback<DummyUser>() {
            @Override
            public void onResponse(Call<DummyUser> call, Response<DummyUser> response) {
                if (null != response) {
                    onSuccess.execute(response.body());
                } else {
                    onFailure.execute(new RestError(RestError.Facility.REST_USER_SERVICE,
                            RestError.ErrorCode.EMPTY_COLLECTION, "Empty data"));
                }
            }

            @Override
            public void onFailure(Call<DummyUser> call, Throwable t) {
                onFailure.execute(new RestError(RestError.Facility.REST_USER_SERVICE,
                        RestError.ErrorCode.INVALID_RESPONSE, t));
            }
        });
    }



    @Override
    public void getUser(final Context context,final String userName,final Callback<DummyUser> onSuccess,final Callback<RestError> onFailure) {

        Call<DummyUser> call = RestClientHolder.getInstance(context).getDataServiceClient(baseUrl).getUser(userName);

        call.enqueue(new retrofit2.Callback<DummyUser>() {
            @Override
            public void onResponse(Call<DummyUser> call, Response<DummyUser> response) {
                if (null != response) {
                    onSuccess.execute(response.body());
                } else {
                    onFailure.execute(new RestError(RestError.Facility.REST_USER_SERVICE,
                            RestError.ErrorCode.EMPTY_COLLECTION, "Empty data"));
                }
            }

            @Override
            public void onFailure(Call<DummyUser> call, Throwable t) {
                onFailure.execute(new RestError(RestError.Facility.REST_USER_SERVICE,
                        RestError.ErrorCode.INVALID_RESPONSE, t));
            }
        });
    }
}
