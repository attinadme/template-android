package com.attinad.baseapp.template.util;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Helper class for keeping common font for Text in all over the application.
 *
 * Uses the constant {@value APPLICATION_FONT} for specifying the application font.
 * Also make sure that following variant of the Font or {@link Typeface} is also available on the asset folder.
 *  1.Typeface.NORMAL
 *  2.Typeface.BOLD
 *  3.Typeface.ITALIC
 *  4.Typeface.BOLD_ITALIC
 *
 *
 * @author SayoojO
 * @since BaseApp
 * <p>
 * Copyright (c) 2017 Attinad Softwares Pvt Lmt. All rights reserved.
 */

public class FontHelper {

    private static final String ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android";

    /**
     * Constant for specifying the Font used through out the application.
     */
    private static final String APPLICATION_FONT = "SourceSansPro";

    /**
     * Method for applying common font for TextView throughout the application.
     *
     * @param textView TextView for which the cont need to be applied.
     * @param context TextView context
     * @param attrs Attribute set from XML.
     */
    public static void applyCustomFont(final TextView textView, final Context context, final AttributeSet attrs) {

        int textStyle = Typeface.NORMAL;
        if (null != attrs){
            textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);
        }

        Typeface customFont = selectTypeface(context, textStyle);

        if(null != customFont){
            textView.setTypeface(customFont);
        }
    }


    /**
     * Method for applying language text for TextView throughout the application.
     *
     * @param textView TextView for which the cont need to be applied.
     * @param context TextView context
     * @param attrs Attribute set from XML.
     */
    public static void applyLanguageText(final TextView textView, final Context context, final AttributeSet attrs) {

        int textStyle = Typeface.NORMAL;
        if (null != attrs){
            textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);
        }

        Typeface customFont = selectTypeface(context, textStyle);

        if(null != customFont){
            textView.setTypeface(customFont);
        }
    }

    /**
     * Method for fetching corresponding  custom font - typeface as specified in the XML.
     *
     * @param context TextView context
     * @param textStyle text style specified in XML.
     * @return Custom-font typeface.
     */
    private static Typeface selectTypeface(Context context, int textStyle) {
    /*
    * information about the TextView textStyle:
    * http://developer.android.com/reference/android/R.styleable.html#TextView_textStyle
    */
        switch (textStyle) {
            case Typeface.BOLD: // bold
                return FontCache.getTypeface(APPLICATION_FONT + "-Bold.ttf", context);

            case Typeface.ITALIC: // italic
                return FontCache.getTypeface(APPLICATION_FONT + "-Italic.ttf", context);

            case Typeface.BOLD_ITALIC: // bold italic
                return FontCache.getTypeface(APPLICATION_FONT + "-BoldItalic.ttf", context);

            case Typeface.NORMAL: // regular
            default:
                return FontCache.getTypeface(APPLICATION_FONT + "-Regular.ttf", context);
        }
    }


}
