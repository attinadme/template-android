package com.attinad.baseapp.template.cache;

import android.content.Context;
import android.databinding.repacked.apache.commons.io.IOUtils;
import android.os.Build;
import android.os.Environment;

import com.attinad.baseapp.template.constants.Constants;
import com.attinad.baseapp.template.util.LogUtil;
import com.jakewharton.disklrucache.DiskLruCache;

import net.jpountz.xxhash.XXHash32;
import net.jpountz.xxhash.XXHashFactory;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;


/**
 * FileCacheManager is an implementation of the {@link CacheManager} interface, backed by the {@link DiskLruCache} by Jake Wharton. When creating a
 * new instance, users can also specify an optional value for time-to-live (TTL) and maximum size. By default, the cache will have a maximum disk
 * reserve of 10 MB and never expire cached objects.
 * <p/>
 * This cache manager accepts a String key and a byte array value. Keys must match the regex [a-z0-9_-]{1,64}. Values are byte sequences, accessible
 * as streams or files. Each value must be between {@code 0} and {@link Integer#MAX_VALUE} bytes in length.
 *
 * @author SayoojO
 * @since BaseApp
 */
public class FileCacheManager implements CacheManager<byte[]> {

    private static final String TAG = "FileCacheManager";

    private static final int MS_IN_ONE_SECOND = 1000;
    private static final long BYTES_IN_ONE_KB = 1024L;

    private static final String EXTERNAL_CACHE_DIR = "assets";
    private static final int APP_VERSION = 1;
    private static final int VALUE_COUNT = 1;

    public static final int DEFAULT_TTL = TTL.FOREVER;
    private static final int DEFAULT_CACHE_SIZE = 10240;

    private static final String SYSTEM_DATA = "/Android/data/";
    private static final String SYSTEM_CACHE_DIR = "/cache/";
    private static final Integer SEED = 0x310530;

    private static final String CACHE_ENTRY_LOG = "cache_entry";
    private static final String EXPIRATION_TIME = "expirationTime";
    private static final String CREATION_TIME = "creationTime";

    private static boolean isExternalStorageRemovable() {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD
                || Environment.isExternalStorageRemovable();
    }

    private static File getExternalCacheDir(final Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            return context.getExternalCacheDir();
        }
        // Before Froyo we need to construct the external cache dir ourselves
        final String cacheDir = SYSTEM_DATA + context.getPackageName() + SYSTEM_CACHE_DIR;
        return new File(Environment.getExternalStorageDirectory().getPath() + cacheDir);
    }

    /**
     * Generates a cache key for use by the {@link FileCacheManager} implementation. This will generate a base 64 key value. This should be used over
     * that provided by .
     *
     * @param endpoint The API endpoint
     * @param path The path to the API method called
     * @return The generated cache key value
     */
    public static String generateCacheKey(final String endpoint, final String path) {
        final String cacheKey = CacheKeyGenerator.generateCacheKey(endpoint, path, null);
        final byte[] cacheKeyInBytes = cacheKey.getBytes(Charset.defaultCharset());
        final XXHash32 xxHash32 = XXHashFactory.safeInstance().hash32();
        int hashInt = xxHash32.hash(cacheKeyInBytes, 0, cacheKeyInBytes.length, SEED);
        if (hashInt < 0) {
            hashInt = -hashInt;
        }
        return Integer.toString(hashInt);
    }


    private final Context context;
    private final long maxCacheSize;
    private final int defaultTtl;
    private final JSONObject cacheEntries;

    private DiskLruCache mDiskCache;

    /**
     * Create a new instance of the {@link FileCacheManager} with default values.
     *
     * @param context The application context
     */
    public FileCacheManager(final Context context, final String directory) {
        this(context, DEFAULT_TTL, DEFAULT_CACHE_SIZE,directory);
    }

    /**
     * Create a new instance and set the maximum size of the cache and the default cache life at the same time.
     * If the ttl is zero, the cache objects never expire.
     *
     * @param context The application context
     * @param ttl The life time of the cached object, in seconds
     * @param maxSize The maximum cache size, in KB
     */
    public FileCacheManager(final Context context, final int ttl, final int maxSize, final String directory) {
        this.context = context;
        defaultTtl = ttl;
        maxCacheSize = maxSize * BYTES_IN_ONE_KB;
        // Find the dir to save cached assets
        try {
            mDiskCache = DiskLruCache.open(getDiskCacheDir(context, EXTERNAL_CACHE_DIR + directory), APP_VERSION, VALUE_COUNT, maxCacheSize);
        } catch (IOException e) {
            LogUtil.e(TAG, "Cannot get the disk cache object for storage.");
        }
        cacheEntries = createEntryLog();
    }

    private File getDiskCacheDir(final Context context, final String dirName) {
        // Check if the media is mounted or storage is built-in; if so, try and use external cache.
        // Otherwise, use the internal cache.
        final String cachePath;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) || !isExternalStorageRemovable()) {
            final File dir = getExternalCacheDir(context);
            if (dir == null) {
                cachePath = context.getCacheDir().getPath();
            } else {
                cachePath = dir.getPath();
            }
        } else {
            cachePath = context.getCacheDir().getPath();
        }
        return new File(cachePath + File.separator + dirName);
    }

    @Override
    public void put(final String key, final byte[] value, final long creationTime) {
        put(key, value, creationTime, defaultTtl);
    }

    @Override
    public void put(final String key, final byte[] value, final long creationTime, final Integer ttl) {
        if (ttl != null && TTL.NEVER.compareTo(ttl) > 0) {
            throw new IllegalArgumentException("The provided ttl value must be positive or provided from the TTL enum");
        }
        if (TTL.NEVER.equals(ttl)) {
            // Never cache the object; return immediately
            return;
        }

        final int ttlValue;
        if (ttl == null || TTL.DEFAULT.equals(ttl)) {
            ttlValue = defaultTtl;
        } else {
            ttlValue = ttl;
        }
        updateLogEntry(key, creationTime, ttlValue);

        // Attempt to write to disk
        try {
            if (mDiskCache != null) {
                final DiskLruCache.Editor editor = mDiskCache.edit(key);
                if (editor == null) {
                    return;
                }
                if (hasWrittenToFile(value, editor)) {
                    mDiskCache.flush();
                    editor.commit();
                } else {
                    editor.abort();
                }
            }
        } catch (IOException e) {
            LogUtil.e(TAG, "Error while getting the key for the data to be stored.");
        } catch (OutOfMemoryError e){
            LogUtil.e(TAG, "OutOfMemory while getting the key for the data to be stored.");
        }
    }

    @Override
    @SuppressWarnings("PMD.ReturnEmptyArrayRatherThanNull")
    public byte[] get(final String key) {
        if (isValid(key)) {
            // Return the valid cache item
            return getValidItem(key);
        }
        // PMD warns that this should return an empty array instead of null;
        // this warning is ignored as the interface requires us to return null if the value is invalid
        return null;
    }

    private byte[] getValidItem(final String key) {
        DiskLruCache.Snapshot snapshot = null;
        try {
            snapshot = mDiskCache.get(key);
            if (snapshot == null) {
                return new byte[0];
            }
            final InputStream inputStream = snapshot.getInputStream(0);
            if (inputStream == null) {
                return new byte[0];
            }
            return IOUtils.toByteArray(inputStream);
        } catch (Exception e) {
            return new byte[0];
        } finally {
            if (snapshot != null) {
                snapshot.close();
            }
        }
    }

    /**
     * Removes the cache entry, specified by the key.
     *
     * @param key The key used to reference the object
     * @return An empty byte array, overriding the default return (which is the removed data)
     */
    @Override
    public byte[] remove(final String key) {
        byte[] value;
        try {
            value = get(key);
            mDiskCache.remove(key);
        } catch (Exception e) {
            value = new byte[0];
        }
        return value;
    }

    @Override
    public boolean contains(final String key) {
        boolean hasObject = false;
        DiskLruCache.Snapshot snapshot = null;
        try {
            if (key != null)
            snapshot = mDiskCache.get(key);
            hasObject = snapshot != null;
        } catch (Exception e) {
            hasObject = false;
        } finally {
            if (snapshot != null) {
                snapshot.close();
            }
        }
        return hasObject;
    }

    @Override
    public boolean isValid(final String key) {
        if (!contains(key)) {
            return false;
        }
        final long expiration = getExpiration(key);
        final boolean isValid = TTL.FOREVER.longValue() == expiration || System.currentTimeMillis() < expiration;
        if (!isValid) {
            // The object exists in the cache, but has expired; remove it
            try {
                mDiskCache.remove(key);
            } catch (IOException e) {
                // There is not much we can do if one of these gets fired off
                LogUtil.w(TAG, "Unable to remove the expired item stored at " + key);
            } catch (OutOfMemoryError e) {
                LogUtil.w(TAG, "Unable to remove the expired item stored at " + key + " due to OutOfMemoryError");
            }
        }
        return isValid;
    }

    private long getExpiration(final String key) {
        // Read from cache entry log file and attempt to get the expiration time for the log entry
        final JSONObject object = cacheEntries.optJSONObject(key);
        if (object != null) {
            return object.optLong(EXPIRATION_TIME);
        }
        return TTL.NEVER;
    }

    @Override
    public long getCreationTime(final String key) {
        // Read from cache entry log file and attempt to get the creation time for the log entry
        if (isValid(key)) {
            final JSONObject object = cacheEntries.optJSONObject(key);
            if (object != null) {
                return object.optLong(CREATION_TIME);
            }
        }
        return 0;
    }

    @Override
    public void clear() {
        try {
            mDiskCache.delete();
            // The library function delete clears and closes the cache; after clearing, it must a new cache must be created
            mDiskCache = DiskLruCache.open(getDiskCacheDir(context, EXTERNAL_CACHE_DIR), APP_VERSION, VALUE_COUNT, maxCacheSize);
        } catch (IOException e) {
            LogUtil.e(TAG, "Error occurred while trying to clear the disk cache");
        } catch (OutOfMemoryError e) {
            LogUtil.e(TAG, "OutOfMemoryError occurred while trying to clear the disk cache");
        }
    }

    @Override
    public void update(final String key, final long creationTime, final Integer ttl) {
        updateLogEntry(key, creationTime, ttl);
    }

    private JSONObject createEntryLog() {
        final byte[] cacheEntriesArr = getValidItem(CACHE_ENTRY_LOG);
        if (cacheEntriesArr.length > 0) {
            try {
                return new JSONObject(new String(cacheEntriesArr, Constants.ENCODING_UTF8));
            } catch (JSONException | UnsupportedEncodingException e) {
                LogUtil.e(TAG, "Error while trying to read the cache entry log file.", e);
            }
        }
        return new JSONObject();
    }

    private void updateLogEntry(final String key, final long creationTime, final long ttl) {
        try {
            final DiskLruCache.Editor editor = mDiskCache.edit(CACHE_ENTRY_LOG);
            if (editor == null) {
                return;
            }
            addLogEntry(key, creationTime, ttl);

            // Update the file containing the cache entry log object
            if (hasWrittenToFile(cacheEntries.toString().getBytes(Charset.defaultCharset()), editor)) {
                mDiskCache.flush();
                editor.commit();
            } else {
                editor.abort();
            }
        } catch (Exception e) {
            LogUtil.e(TAG, "Error occurred when updating the log entry.");
        }
    }

    private void addLogEntry(final String key, final long creationTime, final long ttl) {
        try {
            final JSONObject entryObj = new JSONObject();
            entryObj.put(CREATION_TIME, creationTime);
            if (ttl == 0) {
                // Cache the object forever
                entryObj.put(EXPIRATION_TIME, 0);
            } else {
                entryObj.put(EXPIRATION_TIME, creationTime + (ttl * MS_IN_ONE_SECOND));
            }
            // Update the cache entry log object
            cacheEntries.put(key, entryObj);
        } catch (JSONException e) {
            LogUtil.e(TAG, "Error adding cache entry log");
        }
    }

    private boolean hasWrittenToFile(final byte[] data, final DiskLruCache.Editor editor) {
        if (data != null && data.length > 0) {
            try {
                final OutputStream out = editor.newOutputStream(0);
                out.write(data);
                out.close();
                return true;
            } catch (IOException e) {
                LogUtil.w("File Cache", "Unable to write to file", e);
            }
        }
        return false;
    }
}