package com.attinad.baseapp.template.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Hash {
   
    public static String md5Hash(String toHash)
    {
        String hash = null;
        try
        {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(toHash.getBytes(), 0, toHash.length() );
            hash = new BigInteger( 1, digest.digest() ).toString( 16 );
        }
       // catch(NoSuchAlgorithmException e)
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return hash;
    }

    public static String getMD5Hash(String message)
    {
        String response = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(message.getBytes());
            byte[] digest = md.digest();
            StringBuffer sb = new StringBuffer();
            for (byte b : digest) {
                sb.append(String.format("%02x", b & 0xff));
            }
            response = sb.toString();
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
        return response;
    }
}
