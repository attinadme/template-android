package com.attinad.baseapp.template.service;

import com.attinad.baseapp.template.servicesample.RestUserService;

/**
 * Please replace this comment with meaningfull one.
 *
 * @author SayoojO
 * @since BaseApp
 *
 * Copyright (c) 2017 Attinad Softwares Pvt Lmt. All rights reserved.
 */

public class ServiceHolder {


    private static final String SAMPLE_DATA_API_ENDPOINT ="http://appgrid-api.cloud.accedo.tv/";

    private static volatile ServiceHolder instance;

    private UserService userService;

    /**
     * Returns the singleton instance of {@link ServiceHolder}.
     *
     * @return The ServiceHolder instance
     */
    public static ServiceHolder getInstance() {
        ServiceHolder holder = instance;
        if (holder == null) {
            synchronized (ServiceHolder.class) {
                holder = instance;
                if (holder == null) {
                    holder = new ServiceHolder();
                    instance = holder;
                }
            }
        }
        return holder;
    }

    public UserService getUserService(){
        if (userService == null){
            userService = new RestUserService(SAMPLE_DATA_API_ENDPOINT);
        }
        return userService;
    }


    // Restricting unwanted object creation
    private ServiceHolder() {

    }
}
