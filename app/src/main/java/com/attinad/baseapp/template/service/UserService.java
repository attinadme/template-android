package com.attinad.baseapp.template.service;

import android.content.Context;

import com.attinad.baseapp.template.exception.RestError;
import com.attinad.baseapp.template.servicesample.DummyUser;


/**
 * Please replace this comment with meaningfull one.
 *
 * @author SayoojO
 * @since BaseApp
 * <p>
 * Copyright (c) 2017 Attinad Softwares Pvt Lmt. All rights reserved.
 */

public interface UserService {


    /**
     * create a Dummy user
     *
     * @param context activity context
     * @param userName the username for the user
     * @param onSuccess the DummyUser object
     * @param onFailure the failure callback
     */
    void createUser(final Context context, final String userName, final Callback<DummyUser> onSuccess, final Callback<RestError> onFailure);

    /**
     *  Fetch the user based on the user name
     *
     * @param context activity context
     * @param userName the username of the DummyUser
     * @param onSuccess the success callback with the DummyUser
     * @param onFailure the failure callback
     */
    void getUser(final Context context, final String userName, final Callback<DummyUser> onSuccess, final Callback<RestError> onFailure);




}
