package com.attinad.baseapp.template.exception;

/**
 * The error ({@link java.lang.Exception}) class used for REST Api error handling in applications.
 *
 * @author SayoojO
 * @since BaseApp
 * <p>
 * Copyright (c) 2017 Attinad Softwares Pvt Lmt. All rights reserved.
 */

public class RestError extends Exception {

    /**
     * For having the final five digit error code.
     */
    protected static final int THREE_DIGITS = 1000;
    private static final long serialVersionUID = 7420652319989582909L;

    private final int facility;
    private final int errorCode;

    /**
     * Creates a new TveError instance. This should only be used by subclasses.
     *
     * @param facility The facility code
     * @param errorCode The error code
     * @param message The error message
     */
    public RestError(final int facility, final int errorCode, final String message) {
        super(message);
        this.facility = facility;
        this.errorCode = errorCode;
    }

    /**
     * Creates a new TveError instance. This should only be used by subclasses.
     *
     * @param facility The facility code
     * @param errorCode The error code
     * @param cause The error cause
     */
    public RestError(final int facility, final int errorCode, final Throwable cause) {
        super(cause);
        this.facility = facility;
        this.errorCode = errorCode;
    }

    /**
     * Creates a new TveError instance. This should only be used by subclasses.
     *
     * @param facility The facility code
     * @param errorCode The error code
     * @param message The error message
     * @param cause The error cause
     */
    public RestError(final int facility, final int errorCode, final String message, final Throwable cause) {
        super(message, cause);
        this.facility = facility;
        this.errorCode = errorCode;
    }

    /**
     * Get the error code.
     *
     * @return The error code
     */
    public int getErrorCode() {
        return errorCode;
    }

    /**
     * Get the facility.
     *
     * @return The facility code
     */
    public int getFacility() {
        return facility;
    }

    /**
     * Get the aggregate code for the error.
     *
     * @return The aggregated error code
     */
    public int getCode() {
        return (facility * THREE_DIGITS) + errorCode;
    }



    /**
     * A Facility "enum" class that contains the values for the various facilities.
     * <p/>
     * Android specifications recommend using static final classes instead of enums as a form of optimization.
     */
    public static final class Facility {
        /**
         * Facility code for Content Service (eg. Content Metadata).
         */
        public static final int CONTENT_SERVICE = 11;

        /**
         * Facility code for Rest user Service .
         */
        public static final int REST_USER_SERVICE = 12;

        //Add facility code for each service

        private Facility() {
            // Prevents the creation of a new instance of this "enum"
        }
    }

    /**
     * An Error Code "enum" class that contains the values for the various error codes.
     * <p/>
     * Android specifications recommend using static final classes instead of enums as a form of optimization.
     */
    public static final class ErrorCode {


        /**
         * Code representing instances where the requested item was not found. This is considered a different error case, compared to {@link
         * #INVALID_RESPONSE} or {@link #EMPTY_COLLECTION}; however, the handling of this error may be the same.
         */
        public static final int NOT_FOUND = 1;
        /**
         * Code representing any generic network errors that occurred during communication.
         */
        public static final int NETWORK = 2;
        /**
         * Code representing internal server errors in the backend service.
         */
        public static final int INTERNAL = 3;
        /**
         * Code representing user authorization errors.
         */
        public static final int UNAUTHORIZED = 4;
        /**
         * Code representing errors that may occur during parsing of the response due to invalid data. This is considered a different error case,
         * compared to {@link #NOT_FOUND} or {@link #EMPTY_COLLECTION}; however, the handling of this error may be the same.
         */
        public static final int INVALID_RESPONSE = 5;
        /**
         * Code representing I/O errors during the reading or writing of data, or any other storage resource failures.
         */
        public static final int STORAGE = 6;
        /**
         * Code representing an empty collection. This is considered as a different error case, compared to {@link #INVALID_RESPONSE} or {@link
         * #NOT_FOUND}; however, the handling of this error may be the same.
         */
        public static final int EMPTY_COLLECTION = 7;
        /**
         * Code representing the unavailability of the requested plugin service.
         */
        public static final int SERVICE_UNAVAILABLE = 8;
        /**
         * Code representing any unknown or uncategorized errors. This is used as a catch-all error and is generally not used.
         */
        public static final int UNKNOWN = 911;

        private ErrorCode() {
            // Prevents the creation of a new instance of this "enum"
        }
    }
}
