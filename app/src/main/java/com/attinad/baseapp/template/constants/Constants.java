package com.attinad.baseapp.template.constants;

/**
 * A common place to store the String constants used in the application.
 *
 * @author SayoojO
 * @since BaseApp
 *
 * Copyright (c) 2017 Attinad Softwares Pvt Lmt. All rights reserved.
 */

public interface Constants {

    String ENCODING_UTF8 = "UTF-8";

}
