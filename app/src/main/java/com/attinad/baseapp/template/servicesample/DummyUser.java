package com.attinad.baseapp.template.servicesample;

import com.google.gson.annotations.SerializedName;

/**
 * Please replace this comment with meaningfull one.
 *
 * @author SayoojO
 * @since BaseApp
 * <p>
 * Copyright (c) 2017 Attinad Softwares Pvt Lmt. All rights reserved.
 */

public class DummyUser {


    @SerializedName("userName")
    String mName;

    public DummyUser(String userName ) {
        this.mName = userName;
    }
}
