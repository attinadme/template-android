package com.attinad.baseapp.template.cache;

/**
 * CacheManager is used to manage the cache, with cache life time taken into account.
 * Every cached object will be considered as valid iff it is in the cache and its life isn't past.
 * From time to time, a cached object may be removed by the system or the underlying cache class, based on the caching policy.
 *
 * @param <V> The value type
 * @author SayoojO
 * @since BaseApp
 */
public interface CacheManager<V> {

    /**
     * Put the object to the cache, referenced using the key.
     * This object will be cached for the default interval defined by the manager.
     *
     * @param key The key used to reference the object
     * @param value The object to be cached
     * @param creationTime The time of creation for this cache object
     */
    void put(String key, V value, long creationTime);

    /**
     * Put the object to the cache, referenced using the key.
     * This object will be cached for the interval specified.
     *
     * @param key The key used to reference the object
     * @param value The object to be cached
     * @param creationTime The time of creation for this cache object
     * @param ttl The time-to-life of the cached object, in seconds
     */
    void put(String key, V value, long creationTime, Integer ttl);

    /**
     * Get the object from the cache, referenced by the key.
     * Only valid objects will be returned.
     * If the key cannot be found in the cache, null will be returned.
     *
     * @param key The key used to reference the object
     * @return The object referenced by the key. If no valid object is associated, null will be returned
     */
    V get(String key);

    /**
     * Remove the entry referenced by the key.
     * If the key cannot be found in the cache, nothing will happen on the cache, and null will be returned.
     *
     * @param key The key used to reference the object
     * @return The object referenced by the key. If no object is associated, null will be returned
     */
    V remove(String key);

    /**
     * Check the cache to see if there is an object associating with the key specified.
     *
     * @param key The key used to reference the object
     * @return True if the key is associating with an object in cache, false otherwise
     */
    boolean contains(String key);

    /**
     * Check if the object referenced by the key is a valid copy.
     * Valid copy means that the object exists in the cache, and hasn't expired yet.
     * If no object is associating with the key, false will be returned.
     *
     * @param key The key used to reference the object
     * @return True if valid copy is found, false otherwise
     */
    boolean isValid(String key);

    /**
     * Get the time when this object was put into the cache.
     * This timestamp would be useful for requests with If-Modified-Since header.
     * Every put will refresh the value.
     * If no associated record cannot be found, 0 will be returned.
     *
     * @param key The key used to reference the object
     * @return The check in time of the object, in timestamp (long)
     */
    long getCreationTime(String key);

    /**
     * Clear the entire cache.
     * All item in the cache will be erased.
     */
    void clear();

    /**
     * Edits the cache entry log object, for modifying the creation time and expiration time for a cache entry, specified by its key.
     * When the response code is 304, the cache object does not have to be updated and the cache entry log can be updated with a new expiration time.
     *
     * @param key The key of the cache entry
     * @param creationTime The time at which the entry is created
     * @param ttl The cache life time, in milliseconds
     */
    void update(String key, long creationTime, Integer ttl);

    /**
     * An enum-like representation of possible TTL defaults used for the caching calls.
     */
    final class TTL {

        /**
         * Used to tell the manager to never cache the object.
         */
        public static final Integer NEVER = -2;
        /**
         * Used to tell the manager to never expire the object.
         */
        public static final Integer FOREVER = 0;
        /**
         * Used to tell the manager to use the default TTL value when caching the object.
         */
        public static final Integer DEFAULT = -1;

        private TTL() {
            // Requires no implementation
        }
    }
}