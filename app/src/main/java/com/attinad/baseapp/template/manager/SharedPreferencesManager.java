package com.attinad.baseapp.template.manager;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;

/**
 * SharedPreferencesManager is responsible for the saving and retrieval of application-specific data in the device.
 * The class uses {@link android.content.SharedPreferences} to store data in the form of key-value pairs.
 *
 * @author SayoojO
 * @since BaseApp
 * <p>
 * Copyright (c) 2017 Attinad Softwares Pvt Lmt. All rights reserved.
 */

public class SharedPreferencesManager {

    private final Context context;

    /**
     * Initializes a new {@link SharedPreferencesManager} object.
     *
     * @param context The application context
     */
    public SharedPreferencesManager(final Context context) {
        this.context = context;
    }

    /**
     * Stores the data as a SharedPreferences object, under the specified key.
     *
     * @param key The key used to store the data
     * @param data The data to be stored
     */
    public void savePreferences(final String key, final Map<String, String> data) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        for (final Map.Entry<String, String> s : data.entrySet()) {
            editor.putString(s.getKey(), s.getValue());
        }
        editor.apply();
    }

    /**
     * Returns the data saved under the specified key.
     *
     * @param key The key used to store the data
     * @return The required data
     */
    public Map<String, String> loadPreferences(final String key) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        if (sharedPreferences != null && !sharedPreferences.getAll().isEmpty()) {
            final Map<String, ?> sharedPreferencesMap = sharedPreferences.getAll();
            final Map<String, String> map = new HashMap<>(sharedPreferencesMap.size());
            for (final Map.Entry<String, ?> entry : sharedPreferencesMap.entrySet()) {
                map.put(entry.getKey(), entry.getValue().toString());
            }
            return map;
        }
        return new HashMap<String, String>(0);
    }
}