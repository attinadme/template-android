package com.attinad.baseapp.template.cache;

import java.io.Serializable;
import java.util.Date;

/**
 * CacheEntry represents an object which is stored in a cache.
 * A cache entry contains the actual object to be stored, the check in time, and the expiration.
 * Creation time represents the time when the cached object was put into the cache.
 * Expiration represents the time when the cached object will be considered invalid.
 *
 * @param <T> The data type of the object to be cached
 * @author SayoojO
 * @since BaseApp
 */

public class CacheEntry<T> implements Serializable {

    private static final long serialVersionUID = 652376L;
    private static final int MS_IN_ONE_SECOND = 1000; // number of milliseconds in one second

    private final T object;
    private final long creationTime;
    private long expiration;

    /**
     * Create a new cache entry.
     *
     * @param object The object to cache
     * @param creationTime The time of creation for the object
     * @param ttl The time-to-live for the object, in seconds
     */
    public CacheEntry(final T object, final long creationTime, final Integer ttl) {
        this.object = object;
        this.creationTime = creationTime;
        expiration = creationTime + ttl * MS_IN_ONE_SECOND;
    }

    /**
     * Get the creation time of the object.
     *
     * @return The creation time, in milliseconds
     */
    public long getCreationTime() {
        return creationTime;
    }

    /**
     * Get the cached object.
     *
     * @return The cached object
     */
    public T getObject() {
        return object;
    }

    /**
     * Get the expiration time.
     *
     * @return The expiration time, in milliseconds
     */
    public long getExpiration() {
        return expiration;
    }

    /**
     * Update the expiration time.
     *
     * @param ttl The new time-to-live of the cache object, in seconds
     */
    public void updateTTL(final Integer ttl) {
        expiration = new Date().getTime() + ttl * MS_IN_ONE_SECOND;
    }
}