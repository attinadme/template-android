package com.attinad.baseapp.template.cache;

import android.support.v4.util.LruCache;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * MemoryLruCacheManager is an implementation of the {@link CacheManager} interface. This class is backed by the {@link LruCache} class  from the
 * Android Support Library, which caches objects using the Least-Recently-Used paradigm.
 *
 * @param <V> The data type of the object to be cached
 * @author SayoojO
 * @since BaseApp
 */
public class MemoryLruCacheManager<V> implements CacheManager<V> {

    private static final int MAX_CACHE_SIZE = 2048; //2 MB
    private static final int DEFAULT_CACHE_LIFE = 300; // 5 mins, in sec
    private static final int ONE_KB = 1024; // number of bytes in 1KB

    private final static LruCache<String, CacheEntry<Object>> cache = new LruCache<String, CacheEntry<Object>>(MAX_CACHE_SIZE) {
        @Override
        protected int sizeOf(final String key, final CacheEntry<Object> value) {
            if (value == null) {
                return 0;
            }
            try {
                final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
                // Write the object, then close the output stream
                objectOutputStream.writeObject(value);
                objectOutputStream.flush();
                objectOutputStream.close();
                return (int) Math.ceil((double) byteArrayOutputStream.toByteArray().length / ONE_KB);
            } catch (IOException e) {
                return 0; // cannot read the object correctly
            }
        }
    };
    private final int defaultTtl;

    /**
     * Create a new instance and set the maximum size of the cache at the same time.
     */
    public MemoryLruCacheManager() {
        this(DEFAULT_CACHE_LIFE);
    }

    /**
     * Create a new instance and set the maximum size of the cache and the default cache life at the same time.
     *
     * @param defaultTtl Default life time of the cached object, in seconds
     */
    public MemoryLruCacheManager(final int defaultTtl) {
        this.defaultTtl = defaultTtl;
    }

    @Override
    public void put(final String key, final V value, final long creationTime) {
        cache.put(key, new CacheEntry<Object>(value, creationTime, defaultTtl));
    }

    @Override
    public void put(final String key, final V value, final long creationTime, final Integer ttl) {
        if (ttl != null && TTL.NEVER.compareTo(ttl) > 0) {
            throw new IllegalArgumentException("The provided ttl value must be positive or provided from the TTL enum");
        }
        if (TTL.NEVER.equals(ttl)) {
            // Never cache the object; return immediately
            return;
        }

        final int ttlValue;
        if (ttl == null || TTL.DEFAULT.equals(ttl)) {
            ttlValue = defaultTtl;
        } else if (TTL.FOREVER.equals(ttl)) {
            // Pseudo-forever: MAX_VALUE represents ~68 years in seconds
            ttlValue = Integer.MAX_VALUE;
        } else {
            ttlValue = ttl;
        }
        cache.put(key, new CacheEntry<Object>(value, creationTime, ttlValue));
    }

    @Override
    public V get(final String key) {
        if (isValid(key)) {
            Object value = cache.get(key).getObject();

            try {
                return (V) value;
            }catch (ClassCastException e){
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public V remove(final String key) {
        final CacheEntry<Object> entry = cache.remove(key);
        if (entry == null) {
            return null;
        }

        try {
            return (V) entry.getObject();
        }catch (ClassCastException e){
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean contains(final String key) {
        return cache.get(key) != null;
    }

    @Override
    public boolean isValid(final String key) {
        final CacheEntry<Object> entry = cache.get(key);
        return entry != null && System.currentTimeMillis() < entry.getExpiration();
    }

    @Override
    public long getCreationTime(final String key) {
        final CacheEntry<Object> entry = cache.get(key);
        if (entry == null) {
            return 0;
        }
        return entry.getCreationTime();
    }

    @Override
    public void clear() {
        cache.evictAll();
    }

    @Override
    public void update(final String key, final long creationTime, final Integer ttl) {
        final CacheEntry<Object> entry = cache.get(key);
        if (entry == null) {
            return;
        }
        entry.updateTTL(ttl);
    }
}