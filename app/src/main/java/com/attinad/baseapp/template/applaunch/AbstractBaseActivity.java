package com.attinad.baseapp.template.applaunch;

import android.support.v7.app.AppCompatActivity;

/**
 * A template activity that can be extended by other activities.
 * This activity comes with the simple features like Navigation drawer and Toolbar
 * The customization for Drawer and Toolbar is also added.
 *
 * @author SayoojO
 * @since BaseApp
 * <p>
 * Copyright (c) 2017 Attinad Softwares Pvt Lmt. All rights reserved.
 */

public abstract class AbstractBaseActivity extends AppCompatActivity{

}
