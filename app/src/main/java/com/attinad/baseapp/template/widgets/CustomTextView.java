package com.attinad.baseapp.template.widgets;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.attinad.baseapp.template.util.FontHelper;

/**
 * An Custom TextView for common font & color for throughout the application.
 * This text view also takes care of the Language configuration also.
 *
 * @author SayoojO
 * @since BaseApp
 * <p>
 * Copyright (c) 2017 Attinad Softwares Pvt Lmt. All rights reserved.
 */

public  class CustomTextView extends AppCompatTextView {


    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context,attrs);
    }


    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
    }

    public CustomTextView(Context context) {
        super(context);
        init(context, null);
    }

    private void init(final Context context, final AttributeSet attrs) {
        FontHelper.applyCustomFont(this, context,attrs);

    }

}
