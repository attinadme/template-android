package com.attinad.baseapp.template.util;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.util.ArrayMap;


/**
 * An cache for prevent accessing font assets every time.
 *
 * @author SayoojO
 * @since BaseApp
 * <p>
 * Copyright (c) 2017 Attinad Softwares Pvt Lmt. All rights reserved.
 */
public class FontCache {

    private static ArrayMap<String, Typeface> fontCache = new ArrayMap<>();

    /**
     * Method for returning font typeface either from cache or directly from assets.
     *
     * @param fontName Name of the font
     * @param context View context
     * @return font typeface
     */
    public static Typeface getTypeface(String fontName, Context context) {
        Typeface typeface = fontCache.get(fontName);

        if (typeface == null) {
            try {
                typeface = Typeface.createFromAsset(context.getAssets(), fontName);
            } catch (Exception e) {
                return null;
            }

            fontCache.put(fontName, typeface);
        }

        return typeface;
    }

}
