package com.attinad.baseapp.template.servicesample;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Dummy api interface using retrofit. Delete this.
 *
 * @author SayoojO
 * @since BaseApp
 * <p>
 * Copyright (c) 2017 Attinad Softwares Pvt Lmt. All rights reserved.
 */

public interface DataServiceApiInterface {

    // Request method and URL specified in the annotation
    // Callback for the parsed response is the last parameter

    @GET("users/{username}")
    Call<DummyUser> getUser(@Path("username") String username);


    @POST("users/new")
    Call<DummyUser> createUser(@Body DummyUser user);
}


