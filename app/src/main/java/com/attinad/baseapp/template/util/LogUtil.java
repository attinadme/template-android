package com.attinad.baseapp.template.util;

import android.util.Log;

import com.attinad.baseapp.BuildConfig;

import java.util.Arrays;

/**
 * Simple Lo
 *
 * @author SayoojO
 * @since BaseApp ${PROJECT_VERSION}
 */

public class LogUtil {

    private static final String LOG_FORMAT = "%1$s\n%2$s";
    private static String DEFAULT_TAG = "Wynk";

    public static void d(String message, Object... args) {
        log(Log.DEBUG, message, args);
    }

    public static void i(String message, Object... args) {
        log(Log.INFO, message, args);
    }

    public static void w(String message, Object... args) {
        log(Log.WARN, message, args);
    }

    public static void v(String message, Object... args) {
        log(Log.VERBOSE, message, args);
    }

    public static void e(String message, Object... args) {
        log(Log.ERROR, message, args);
    }


    private static void log(int priority, String tag, Object... args) {
        if (BuildConfig.DEBUG) {

            if (tag == null)
                tag = DEFAULT_TAG;

            String message = null;

            if (args != null && args.length > 0) {
                try {
                    message = Arrays.toString(args).replaceAll("(^\\[|\\]$)", ""); //Convert object array to string and remove leading and trailing square brackets
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            if (message==null) return;

            switch (priority) {
                case Log.ERROR:
                    Log.e(tag, message);
                    break;
                case Log.DEBUG:
                    Log.d(tag, message);
                    break;
                case Log.VERBOSE:
                    Log.v(tag, message);
                    break;
                case Log.WARN:
                    Log.w(tag, message);
                    break;
                case Log.INFO:
                    Log.i(tag, message);
                    break;
            }
        }
    }

}

